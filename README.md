# bl_order2wl.html - Introduction

bl_order2wl.html is a standalone HTML/ECMAScript application to convert a
BrickLink™ XML order into a BrickLink™ XML Wanted List.

By “standalone application” we mean that the user just needs to open the file
bl_order2wl.html in their (recent) browser.  No web server needed.  No other
installation than downloading the files.

bl_ord2wl.html is licenced under GPLv3+.


# Requirements

* NONE!


LEGO and BrickLink are trademarks of the LEGO Group, which does not sponsor,
endorse, or authorize this application.

# How to use
1. Export your orders from Bricklink:
    1. Login to your account at Bricklink
    1. Go to *My Orders*
    1. Below the table with orders is link *Download*, click on it.
    1. Select *File Format: XML*.
    1. Check box next to *Include detail items*.
    1. Set proper date *From* and *To* so all required orders are included.
    1. Click button *Download My Orders*.
    1. Save file *orderxml.txt* to your computer.
1. Download file *bl_order2wl.html* from this project to your computer.
1. Open the downloaded html file in your internet browser (in browser press *Ctrl+O* and select the
   downloaded *bl_order2wl.html* file.
1. In your internet browser showing the *bl_order2wl.html*, click button *Choose Files*.
1. Select file with orders *orderxml.txt*.
1. Select *Download results for xxxxxx* according the required order (xxxxxx is number of the
   order).
1. Save the resulting *xxxxxx.xml* file into your computer.
1. Upload the *xxxxxx.xml* as wanted list at Bricklink:
    1. Go to *Wanted Lists* at Bricklink.
    1. Select *Upload*.
    1. Select *Upload BrickLink XML format*.
    1. Open file *xxxxxx.xml* in text editor (e.g. Notepad).
    1. Select all text in text editor and copy (*Ctrl+C*).
    1. Paste all text into text box *Copy and paste here* at bricklink.
    1. Click button *Proceed to verify items*.
